import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor() {}

  /**
   * Interceptor clones the outgoing request and adds extra information to the requests.
   * @param request HttpRequest
   * @param next HttpHandler
   * @returns intercept
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem("token"));
    if (token) {
      const sendToken = request.clone({
        setHeaders: { "x-api-token": token }
      });
      return next.handle(sendToken);
    } else {
      return next.handle(request);
    }
  }
}
